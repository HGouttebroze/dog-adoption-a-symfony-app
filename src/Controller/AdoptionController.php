<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdoptionController extends AbstractController
{
    /**
     * @Route("/annonces", name="adoption_index")
     */
    public function index(): Response
    {
        $dogs = [
          [
              'name' => 'Moon',
              'img' => 'https://images.unsplash.com/photo-1534351450181-ea9f78427fe8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8ZG9nc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
          ],
          [
              'name' => 'KiKo',
              'img' => 'https://images.unsplash.com/photo-1518717758536-85ae29035b6d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZG9nc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
          ],
        ];
        return $this->render('adoption/index.html.twig', [
            'dogs' => $dogs,
        ]);
    }
}
